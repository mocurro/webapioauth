﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http;

namespace WebApiOauth2.Controllers
{
    [Authorize]
    public class WebApiController : ApiController
    {

        String strconexion = ConfigurationManager.ConnectionStrings["ConexionTI"].ConnectionString;

        public string Get(string SapUsuario)
        {
            string Resultado = "";
            using (SqlConnection cnx = new SqlConnection(strconexion))
            {
                cnx.Open();
                using (SqlCommand objCmd = new SqlCommand())
                {
                    objCmd.Connection = cnx;

                    objCmd.CommandText = string.Format("SELECT * FROM Usuarios WHERE Codigo='{0}' and (Eliminado <> 'SI' OR Eliminado IS NULL) ;", SapUsuario);

                    SqlDataReader registros = objCmd.ExecuteReader();
                    while (registros.Read())
                    {
                        Resultado = string.Format("ID:{0},NOMBRE:{1},CODIGO:{2},CLAVE:{3}", int.Parse(registros["IdUsuario"].ToString()), registros["Nombre"].ToString(), registros["Codigo"].ToString(), registros["Clave"].ToString());

                        break;
                    }
                    registros.Close();
                }
            }
            return Resultado;
        }


        // PUT api/WebApi/5
        public string Put(string GuidTag, DateTime Fecha, double Latitud, double Longitud, double Precision, string SAPRuta, string SAPUsuario,int TipoIncidencia)
        {

            int IdCliente = 0, idRuta = 0, idUsuario = 0, idTipoIncidencia=0;
            string Resultado = "";
            try
            {
                if (TipoIncidencia > 0)
                {
                    using (SqlConnection cnx = new SqlConnection(strconexion))
                    {
                        cnx.Open();
                        using (SqlCommand objCmd = new SqlCommand())
                        {
                            objCmd.Connection = cnx;
                            objCmd.CommandText = string.Format("select IdTipoIncidencia from TipoIncidencia where IdIncidencia = '{0}';", TipoIncidencia);
                            SqlDataReader registros = objCmd.ExecuteReader();
                            while (registros.Read())
                            {
                                idTipoIncidencia = int.Parse(registros["IdTipoIncidencia"].ToString());

                                break;
                            }
                            registros.Close();
                        }
                    }
                }
                using (SqlConnection cnx = new SqlConnection(strconexion))
                {
                    cnx.Open();
                    using (SqlCommand objCmd = new SqlCommand())
                    {
                        objCmd.Connection = cnx;
                        objCmd.CommandText = string.Format("select cli.idcliente  from clientes cli join tags tg on tg.idtag = cli.idtag where tg.mitag = '{0}' and MiTag<>'';", GuidTag);
                        SqlDataReader registros = objCmd.ExecuteReader();
                        while (registros.Read())
                        {
                            IdCliente = int.Parse(registros["idcliente"].ToString());

                            break;
                        }
                        registros.Close();
                    }
                }
                if (IdCliente > 0)
                {
                    DataTable dt = new DataTable("Tabla");
                    try
                    {
                        using (SqlConnection cnx = new SqlConnection(strconexion))
                        {
                            cnx.Open();
                            using (SqlCommand objCmd = new SqlCommand())
                            {
                                objCmd.Connection = cnx;
                                objCmd.CommandText = string.Format("SELECT IdRuta FROM Rutas WHERE Codigo='{0}' and (Eliminado <> 'SI' OR Eliminado IS NULL) ;", SAPRuta);
                                SqlDataReader registros = objCmd.ExecuteReader();
                                while (registros.Read())
                                {
                                    idRuta = int.Parse(registros["IdRuta"].ToString());
                                    break;
                                }
                                registros.Close();
                            }
                            if (idRuta > 0)
                            {
                                using (SqlCommand objCmd = new SqlCommand())
                                {
                                    objCmd.Connection = cnx;
                                    objCmd.CommandText = string.Format("SELECT top 1 IdUsuario FROM Usuarios WHERE IdRuta='{0}';", idRuta);
                                    SqlDataReader registros = objCmd.ExecuteReader();
                                    while (registros.Read())
                                    {
                                        idUsuario = int.Parse(registros["IdUsuario"].ToString());
                                        break;
                                    }
                                    registros.Close();
                                }

                                if (idUsuario > 0)
                                {
                                    using (SqlCommand objCmd = new SqlCommand())
                                    {

                                        objCmd.Connection = cnx;
                                        //objCmd.CommandTimeout = 0;
                                        objCmd.CommandText = "[dbo].[VD_SP_AgregaRegistroV4]";
                                        objCmd.CommandType = CommandType.StoredProcedure;

                                        objCmd.Parameters.Add("@Latitud", SqlDbType.Float).Value = Latitud;
                                        objCmd.Parameters.Add("@Longitud", SqlDbType.Float).Value = Longitud;
                                        objCmd.Parameters.Add("@IdCliente", SqlDbType.Int).Value = IdCliente;
                                        objCmd.Parameters.Add("@IdUsuario", SqlDbType.Int).Value = idUsuario;
                                        objCmd.Parameters.Add("@IdRuta", SqlDbType.Int).Value = idRuta;
                                        objCmd.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = Fecha.ToString("yyyy-MM-dd HH:mm:ss");
                                        objCmd.Parameters.Add("@Precision", SqlDbType.Float).Value = Precision;
                                        objCmd.Parameters.Add("@GuidTag", SqlDbType.NVarChar).Value = GuidTag;
                                        objCmd.Parameters.Add("@IdIncidencia", SqlDbType.Int).Value = idTipoIncidencia;

                                        int res = objCmd.ExecuteNonQuery();
                                        cnx.Close();
                                        if (res > 0)
                                            Resultado = "CD000";
                                    }
                                }
                                else
                                {
                                    Resultado = "Error; Código de usuario incorrecto.";
                                    Resultado = "CD001";




                                }


                            }

                            else
                            {
                                Resultado = "Error; Código de ruta incorrecto.";
                                Resultado = "CD002";
                            }


                            cnx.Close();
                        }
                    }
                    catch (Exception ex)

                    {
                        //Resultado = "Error, " + ex.Message+ex.StackTrace ;
                        Resultado = "CD003";
                    }
                }
                else
                {
                    Resultado = "Error; Cliente no existe.";
                    Resultado = "CD004";
                }
            }
            catch (Exception ex)
            {
                Resultado = string.Format("Error; {0}", ex.Message);
                Resultado = "CD005";
            }
            return Resultado;

        }

        // DELETE api/WebApi/5
        public void Delete(int id)
        {
        }
    }
}
